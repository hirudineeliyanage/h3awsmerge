let AWS = require('aws-sdk');
const sns = new AWS.SNS();

exports.handler = function(event, context, callback) {
    sns.publish({
  Message: 'test ',
  Subject: 'sample',
  MessageAttributes: {},
  MessageStructure: 'String',
  TopicArn: 'arn:aws:sns:us-east-2:318300609668:dynamodb'
}).promise()
                .then(data => {
                    // your code goes here edit 123
                })
                .catch(err => {
                    // error handling goes here
                });
    
    callback(null, {"message": "Successfully executed"});
}